const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const fs = require('fs');
const signaling = require('./signaling');
const log = require('./log');

exports.createServer = function (config) {
  const app = express();
  // const signal = require('./signaling');
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  app.get('/protocol', function (req, res) {
    return res.json({ useWebSocket: config.websocket });
  });
  app.use('/signaling', signaling.default);
  app.use(express.static(path.join(__dirname, '/../web/public/stylesheets')));
  app.use(express.static(path.join(__dirname, '/../web/public/scripts')));
  app.use(
    '/images',
    express.static(path.join(__dirname, '/../web/public/images'))
  );
  app.get('/', function (req, res) {
    var indexPagePath = path.join(__dirname, '/../web/public/index.html');
    fs.access(indexPagePath, function (err) {
      if (err) {
        log.log(log.LogLevel.warn, "Can't find file ' " + indexPagePath);
        res.status(404).send("Can't find file " + indexPagePath);
      } else {
        res.sendFile(indexPagePath);
      }
    });
  });
  return app;
};
