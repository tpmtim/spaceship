# spaceship-rpi

This is a project to build a spaceship cockpit for my son, based on a Raspberry Pi, multiple Arduino, tons of lights, switches and co, medium electronic skills, limited soldering and super beginner woodworking skills.

The idea is to start with 4 different panels who will contain different hardware elements, buttons, switches, joysticks, led panels, numpad, rfid chip that are wired up to arduino's which then talk to one master Raspberry Pi which has a screen and is running a Web App to simulate space flight. 

## Panels

Every panel is 20x20 cm in size. 

### Panel 1

![Panel 1](design/panels/panel_1.png)

### Panel 2

![Panel 1](design/panels/panel_2.png)

### Panel 3

![Panel 1](design/panels/panel_3.png)

### Panel 4

![Panel 1](design/panels/panel_4.png)

## Keyboard mapping

Arrow Keys - Joystick
Numpad - Color Buttons

K - NFC Keyring
T - Target Switch
O - Oxygen
I - Ignition Switch
M - Music Switch

L - Lights Switch
R - Radar Switch
P - Toilet Switch
N - Robot Switch
S - Send Image Switch



# Streaming Setup

Using moonlight to stream Unity Game from PC to Raspberry Pi with command

``moonlight stream -app RocketUnity``

Getting the screen resolution on RPI

``fbset -s`` 

# Video Loop Credits

Seconds from SpaceX CRS-18 Cargo Launch - https://www.youtube.com/watch?v=nPbA9-oLDI8 
