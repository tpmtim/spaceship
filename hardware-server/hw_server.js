const { wsServer } = require('./hw_ws_server');
const { globalEvents } = require('../utils/global_events.js');

// require('events').EventEmitter.defaultMaxListeners = 150;

const { Boards } = require('johnny-five');
const { RaspiIO } = require('raspi-io');

const SoundPanel = require('./panels/panel_sounds.js');
const MiniDisplayPanel = require('./panels/panel_mini_display.js');
const JoystickPanel = require('./panels/panel_joystick.js');
const LightsPanel = require('./panels/panel_lights.js');

const keyCard = require('./keycard.js');

console.log('HW Server starts');

const initBoards = true;

if (initBoards) {
  const boards = new Boards([
    { id: 'ARDURIGHT', repl: false },
    { id: 'ARDULEFT', repl: false },
  ]);

  //,

  // { id: 'RPI', io: new RaspiIO(), repl: false }

  boards.on('ready', () => {
    const soundPanel = new SoundPanel(boards.byId('ARDULEFT'));
    soundPanel.setUpDevices();

    const miniDisplayPanel = new MiniDisplayPanel(boards.byId('ARDULEFT'));
    miniDisplayPanel.setUpDevices();

    const joystickPanel = new JoystickPanel(boards.byId('ARDURIGHT'));
    joystickPanel.setUpDevices();

    const lightsPanel = new LightsPanel(boards.byId('ARDURIGHT'));
    lightsPanel.setUpDevices();

    console.log('BOARDS READY');
    globalEvents.emit('sendws', 'boards-ready');
  });

  // Allow simulated entering of data
  var stdin = process.openStdin();

  stdin.addListener('data', function (d) {
    // note:  d is an object, and when converted to a string it will
    // end with a linefeed.  so we (rather crudely) account for that
    // with toString() and then trim()

    const message = d.toString().trim();

    if (message.indexOf('|') > -1) {
      const msgParts = message.split('|');
      globalEvents.emit(msgParts[0], message.substr(message.indexOf('|') + 1));
    }
  });
}
