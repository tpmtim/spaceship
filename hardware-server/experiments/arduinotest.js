const { Board, Led } = require('johnny-five');

const board = new Board({ id: 'ARDU', port: '/dev/ttyACM0' });

let led;

board.on('ready', () => {
  led = new Led(5);
  led.blink(500);

  board.on('exit', function() {
    led.stop();
    led.off();
  });
});
