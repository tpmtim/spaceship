const { Board, Led } = require('johnny-five');
const { RaspiIO } = require('raspi-io');
const rpi = new Board({
  id: 'RPI',
  io: new RaspiIO()
});

rpi.on('ready', () => {
  const led = new Led('GPIO23');
  led.blink(500);
});
