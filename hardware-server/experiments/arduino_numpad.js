const { Board, Pin } = require('johnny-five');

const board = new Board();

board.on('ready', function() {
  const row = [2, 3, 4, 5];
  const col = [6, 7, 8, 9];

  /* Configuration */
  for (let r = 0; r < row.length; r++) {
    this.pinMode(row[r], Pin.OUTPUT);
    this.digitalWrite(row[r], 1);
  }

  for (let c = 0; c < row.length; c++) {
    this.pinMode(col[c], Pin.INPUT);
    this.digitalWrite(col[c], 1);
  }

  function sleep(miliseconds) {
    var currentTime = new Date().getTime();

    while (currentTime + miliseconds >= new Date().getTime()) {}
  }

  const b = this;

  const readNumPad = function() {
    console.log('READ NUMPAD');
    for (let r = 0; r < row.length; r++) {
      b.digitalWrite(row[r], 0);
      for (let c = 0; c < col.length; c++) {
        sleep(10);

        if (!b.pins[col[c]].value) {
          console.log('C ' + c + '/' + r + ': ' + b.pins[col[c]].value);
          b.digitalWrite(row[r], 1);

          // return c + ' / ' + r;
        }
      }
      b.digitalWrite(row[r], 1);
    }
    return '';
  };
  const debounceTime = 300; // ms

  console.log('INIT DONE');
  setInterval(readNumPad, 500);
});
