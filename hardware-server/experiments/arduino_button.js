const { Board, Led, Button } = require('johnny-five');

const board = new Board();

let ledstate = false;

board.on('ready', function() {
  const led = new Led(13);
  led.off();

  // Create a new `button` hardware instance.
  // This example allows the button module to
  // create a completely default instance
  const button = new Button(2);

  // Inject the `button` hardware into
  // the Repl instance's context;
  // allows direct command line access
  board.repl.inject({
    button: button
  });

  // Button Event API

  // "down" the button is pressed
  button.on('down', function() {
    // led.off();
    console.log('down ' + ledstate);
    ledstate = !ledstate;
    if (ledstate) {
      led.on();
    } else {
      led.off();
    }
  });

  // "hold" the button is pressed for specified time.
  //        defaults to 500ms (1/2 second)
  //        set
  button.on('hold', function() {
    console.log('hold');
  });

  // "up" the button is released
  button.on('up', function() {
    console.log('up');
    // led.on();
  });
});
