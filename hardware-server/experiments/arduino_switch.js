const { Board, Switch } = require('johnny-five');

const board = new Board();

let ledstate = false;

board.on('ready', function() {
  // Options object with pin property
  const mySwitch = new Switch({
    pin: 11
  });

  mySwitch.on('open', function() {
    console.log('OPEN');
  });

  mySwitch.on('close', function() {
    console.log('CLOSE');
  });
});
