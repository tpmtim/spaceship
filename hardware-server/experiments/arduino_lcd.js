const { Board, LCD } = require('johnny-five');

const board = new Board();

board.on('ready', function() {
  // Controller: PCF8574A (Generic I2C)
  // Locate the controller chip model number on the chip itself.
  var l = new LCD({
    controller: 'PCF8574T'
  });
  l.cursor(0, 0);
  l.print('ASTRONAUT XAVER!');
  l.cursor(1, 0);
  l.print('STARTE DIE MOTOREN');

  setTimeout(function() {
    //process.exit(0);
  }, 3000);
});
