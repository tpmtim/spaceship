const { Board, Joystick } = require('johnny-five');

const board = new Board();

board.on('ready', function() {
  // Create a new `joystick` hardware instance.
  var joystick = new Joystick({
    //   [ x, y ]
    pins: ['A0', 'A1']
  });

  joystick.on('change', function() {
    console.log('Joystick');
    console.log('  x : ', this.x);
    console.log('  y : ', this.y);
    console.log('--------------------------------------');
  });
});
