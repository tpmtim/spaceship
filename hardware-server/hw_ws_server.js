const WebSocket = require('ws');
const { globalEvents } = require('../utils/global_events.js');

console.log('WS Server starts');

const wss = new WebSocket.Server({
  port: 8080,
});

wss.on('connection', (ws) => {
  ws.on('message', (message) => {
    console.log('received: %s', message);
    if (message.indexOf('|') > -1) {
      const msgParts = message.split('|');
      globalEvents.emit(msgParts[0], message.substr(message.indexOf('|') + 1));
    } else {
      globalEvents.emit('msg', message);
    }
  });

  globalEvents.on('sendws', (msg) => {
    console.log('SEND WS : ', msg);
    ws.send(msg);
  });
});

exports.wsServer = wss;
