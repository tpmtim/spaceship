const { Led, LCD, Switch, Sensor } = require('johnny-five');
const _ = require('lodash');
const LedFourDigits = require('./subcomponents/led_four_digits');

const { globalEvents } = require('../../utils/global_events.js');

class MiniDisplayPanel {
  constructor(initialisedBoard) {
    this.board = initialisedBoard;
    this.ledstate = false;
  }
  setUpDevices() {
    const panel = this;

    console.log('Setting up Display PANEL');
    /*const potentiometer = new Sensor('A0');
    potentiometer.scale(0, 255).on('change', function () {
      console.log('VAL ' + potentiometer.value);
    });*/

    this.height_display = new LedFourDigits({
      board: this.board,
      pinData: 34,
      pinClock: 35,
    });
    this.height_display.print('0000');

    let height = 0;
    let heightdisp;
    globalEvents.on('setheight', (msg) => {
      const target_height = Number(msg);
      clearInterval(heightdisp);

      heightdisp = setInterval(() => {
        if (target_height > height) {
          height += 1;
        } else if (target_height < height) {
          height -= 1;
        } else {
          clearInterval(heightdisp);
        }

        this.height_display.print(_.padStart(height.toString(), 4, '0'));
      }, 1);
    });

    globalEvents.on('setheightexact', (newHeight ) => {
      height = newHeight;
      this.height_display.print(_.padStart(height.toString(), 4, '0'));
    });

    var l = new LCD({
      board: this.board,
      controller: 'PCF8574T',
    });
    l.cursor(0, 0);
    l.print('ASTRONAUT XAVER!');
    l.cursor(1, 0);
    l.print('STARTE DEN MOTORsosss');

    this.led_numpad_orange = this.setupNewLed(50);
    this.led_numpad_red = this.setupNewLed(51);
    this.led_key = this.setupNewLed(52);

    globalEvents.on('displaypanel', (msg) => {
      console.log('DISPLAY : ' + msg);
      const msgParts = msg.split('|');
      if (msgParts.length > 1) {
        console.log('LED ACTION ' + msgParts[0]);
        if (panel['led_' + msgParts[0]]) {
          panel['led_' + msgParts[0]][msgParts[1]]();
        } else {
          console.warn(msgParts[0] + ' doesnt exist on displaypanel');
        }
      } else if (msg == 'allOn') {
        panel.allLedOn();
      } else if (msg == 'allOff') {
        panel.allLedOff();
      }
    });

    this.board.on('exit', function () {
      console.log('Exiting Display Panel');
      panel.allLedOff();
    });
  }
  setupNewLed(selectedPin) {
    let newLed = new Led({
      board: this.board,
      pin: selectedPin,
    });
    newLed.off();

    return newLed;
  }
  allLedOn() {
    console.log('LightsPanel : All On');
    this.led_numpad_orange.on();
    this.led_numpad_red.on();
    this.led_key.on();
  }
  allLedOff() {
    console.log('LightsPanel : All Off');
    this.led_numpad_orange.off();
    this.led_numpad_red.off();
    this.led_key.off();
  }
}

module.exports = MiniDisplayPanel;
