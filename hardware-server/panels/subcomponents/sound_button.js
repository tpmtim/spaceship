const { Led, Button } = require('johnny-five');
const { globalEvents } = require('../../../utils/global_events.js');

class SoundButton {
  constructor(options) {
    this.id = options.id;
    this.board = options.board;
    this.buttonPin = options.buttonPin;
    this.ledPin = options.ledPin;
    this.soundFile = options.soundFile;

    this.ledstate = false;

    console.log('Setting up SOUND BUTTON ' + this.id);

    this.led = new Led({
      board: this.board,
      pin: this.ledPin,
    });
    this.led.off();

    this.button = new Button({
      board: this.board,
      pin: this.buttonPin,
    });

    this.button.on('down', () => {
      console.log('SOUND BUTTON : ' + this.id);
      globalEvents.emit('sendws', 'sound-button|' + this.id);
    });
  }
  stop() {
    console.log('Stopping: ' + this.id);
    this.led.stop();
    this.led.off();
  }
}

module.exports = SoundButton;
