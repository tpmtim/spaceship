const five = require('johnny-five');
const { Led, Switch } = require('johnny-five');
const _ = require('lodash');
const LedFourDigits = require('./subcomponents/led_four_digits');
const { globalEvents } = require('../../utils/global_events.js');

class LightsPanel {
  constructor(initialisedBoard) {
    this.board = initialisedBoard;
    this.ledstate = false;
  }
  setUpDevices() {
    const panel = this;

    console.log('Setting up Lights PANEL');

    this.left_clock = new LedFourDigits({
      board: this.board,
      pinData: 29,
      pinClock: 28,
    });
    this.left_clock.print('00:00');

    this.speed_display = new LedFourDigits({
      board: this.board,
      pinData: 53,
      pinClock: 52,
    });
    this.speed_display.print('0000');

    let speed = 0;
    let speeddisp;
    globalEvents.on('setspeed', (val) => {
      const target_speed = Number(val);
      clearInterval(speeddisp);

      speeddisp = setInterval(() => {
        if (target_speed > speed) {
          speed += 1;
        } else if (target_speed < speed) {
          speed -= 1;
        } else {
          clearInterval(speeddisp);
        }

        this.speed_display.print(_.padStart(speed.toString(), 4, '0'));
      }, 1);
    });

    globalEvents.on('setspeedexact', (newSpeed ) => {
      speed = newSpeed;
      this.speed_display.print(_.padStart(speed.toString(), 4, '0'));
    });

    /*
    this.left_clock_2 = new LedFourDigits({
      board: this.board,
      pinData: 50,
      pinClock: 51,
    });
    this.left_clock_2.print('00:03');*/

    /*this.left_clock_3 = new LedFourDigits({
      board: this.board,
      pinData: 48,
      pinClock: 49,
    });
    this.left_clock_3.print('00:04');*/

    this.led_camera = this.setupNewLed(2);
    this.led_booster = this.setupNewLed(3);
    this.led_fire = this.setupNewLed(4);
    this.led_battery = this.setupNewLed(5);
    this.led_com = this.setupNewLed(6);
    this.led_toilet = this.setupNewLed(7);
    this.led_robot_power = this.setupNewLed(8);
    this.led_dinner = this.setupNewLed(9);
    this.led_alarm = this.setupNewLed(10);

    globalEvents.on('lightspanel', (msg) => {
      console.log('LIGHTS : ' + msg);
      const msgParts = msg.split('|');
      if (msgParts.length > 1) {
        console.log('LED ACTION ' + msgParts[0]);
        panel['led_' + msgParts[0]][msgParts[1]]();
      } else if (msg == 'allOn') {
        panel.allLedOn();
      } else if (msg == 'allOff') {
        panel.allLedOff();
      }
    });

    this.switch_image = this.setupNewSwitch(22, 'image');
    this.switch_robot = this.setupNewSwitch(23, 'robot');
    this.switch_toilet = this.setupNewSwitch(24, 'toilet');
    this.switch_radar = this.setupNewSwitch(25, 'radar');
    this.switch_lights = this.setupNewSwitch(26, 'lights');

    this.board.on('exit', function () {
      console.log('Exiting Lights Panel');
      panel.allLedOff();
      panel.left_clock.off();
      panel.speed_display.off();
    });
  }
  setupNewLed(selectedPin) {
    let newLed = new Led({
      board: this.board,
      pin: selectedPin,
    });
    newLed.off();

    return newLed;
  }
  setupNewSwitch(selectedPin, switchId) {
    const newSwitch = new Switch({
      board: this.board,
      pin: selectedPin,
    });

    let switchValue = null;

    newSwitch.on('open', () => {
      if (switchValue != 'on') {
        switchValue = 'on';
        console.log('SWITCH ' + switchId + ' ON');
        globalEvents.emit('sendws', 'switch-on|' + switchId);
      }
    });
    newSwitch.on('close', () => {
      if (switchValue != 'off') {
        switchValue = 'off';
        console.log('SWITCH ' + switchId + ' OFF');
        globalEvents.emit('sendws', 'switch-off|' + switchId);
      }
    });

    return newSwitch;
  }
  allLedOn() {
    console.log('LightsPanel : All On');
    this.led_camera.on();
    this.led_booster.on();
    this.led_fire.on();
    this.led_battery.on();
    this.led_com.on();
    this.led_toilet.on();
    this.led_robot_power.on();
    this.led_dinner.on();
    this.led_alarm.on();
  }
  allLedOff() {
    console.log('LightsPanel : All Off');
    this.led_camera.off();
    this.led_booster.off();
    this.led_fire.off();
    this.led_battery.off();
    this.led_com.off();
    this.led_toilet.off();
    this.led_robot_power.off();
    this.led_dinner.off();
    this.led_alarm.off();
  }
}

module.exports = LightsPanel;
