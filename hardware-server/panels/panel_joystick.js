const { Led, Button, Switch, Sensor } = require('johnny-five');
const _ = require('lodash');

const { globalEvents } = require('../../utils/global_events.js');

class JoystickPanel {
  constructor(initialisedBoard) {
    this.board = initialisedBoard;
    this.ledstate = false;
  }
  setUpDevices() {
    console.log('Setting up Joystick PANEL');

    this.left_slider = new Sensor({ board: this.board, pin: 'A0' });
    this.left_slider.scale([0, 100]).on('change', () => {
      //console.log('left slide', panel.left_slider.value);
    });
    this.right_slider = new Sensor({ board: this.board, pin: 'A1' });
    this.right_slider.scale([0, 100]).on('change', () => {
      //console.log('right slide', panel.right_slider.value);
    });

    this.joystick_up = this.setupNewJoystickButton(39, 'up');
    this.joystick_down = this.setupNewJoystickButton(37, 'down');
    this.joystick_left = this.setupNewJoystickButton(36, 'left');
    this.joystick_right = this.setupNewJoystickButton(38, 'right');

    this.switch_target = this.setupNewSwitch(44, 'target');
    this.switch_view = this.setupNewSwitch(45, 'view');
    this.switch_music = this.setupNewSwitch(42, 'music');
    this.switch_ignition = this.setupNewSwitch(43, 'ignition');

    const panel = this;

    this.board.on('exit', function () {
      console.log('Exiting Joystick Board');
    });
  }
  setupNewJoystickButton(selectedPin, direction) {
    const newButton = new Button({
      board: this.board,
      pin: selectedPin,
    });

    newButton.on('down', () => {
      console.log('joystick ' + direction + ' button');
    });

    return newButton;
  }
  setupNewSwitch(selectedPin, switchId) {
    const newSwitch = new Switch({
      board: this.board,
      pin: selectedPin,
    });

    let switchValue = null;

    newSwitch.on('open', () => {
      if (switchValue !== 'on') {
        switchValue = 'on';
        console.log('RED SWITCH ' + switchId + ' ON');
        globalEvents.emit('sendws', 'red-switch-on|' + switchId);
      }
    });
    newSwitch.on('close', () => {
      if (switchValue !== 'off') {
        switchValue = 'off';
        console.log('RED SWITCH ' + switchId + ' OFF');
        globalEvents.emit('sendws', 'red-switch-off|' + switchId);
      }
    });

    return newSwitch;
  }
}

module.exports = JoystickPanel;
