const { Led, Button } = require('johnny-five');
const { globalEvents } = require('../../utils/global_events.js');
const SoundButton = require('./subcomponents/sound_button.js');

class SoundPanel {
  constructor(initialisedBoard) {
    this.board = initialisedBoard;
    this.ledstate = false;
  }
  setUpDevices() {
    console.log('Setting up SOUND PANEL');

    this.button1 = new SoundButton({
      id: '1',
      board: this.board,
      buttonPin: 2,
      ledPin: 22,
      soundFile: 'thruster',
    });

    this.button2 = new SoundButton({
      id: '2',
      board: this.board,
      buttonPin: 3,
      ledPin: 23,
      soundFile: 'rocket',
    });

    this.button3 = new SoundButton({
      id: '3',
      board: this.board,
      buttonPin: 4,
      ledPin: 24,
      soundFile: 'rocket',
    });

    this.button4 = new SoundButton({
      id: '4',
      board: this.board,
      buttonPin: 5,
      ledPin: 25,
      soundFile: 'bleep',
    });

    this.button5 = new SoundButton({
      id: '5',
      board: this.board,
      buttonPin: 6,
      ledPin: 26,
      soundFile: 'rocket',
    });

    this.button6 = new SoundButton({
      id: '6',
      board: this.board,
      buttonPin: 7,
      ledPin: 27,
      soundFile: 'rocket',
    });

    this.button7 = new SoundButton({
      id: '7',
      board: this.board,
      buttonPin: 8,
      ledPin: 28,
      soundFile: 'rocket',
    });

    this.button8 = new SoundButton({
      id: '8',
      board: this.board,
      buttonPin: 9,
      ledPin: 29,
      soundFile: 'rocket',
    });

    this.button9 = new SoundButton({
      id: '9',
      board: this.board,
      buttonPin: 10,
      ledPin: 30,
      soundFile: 'rocket',
    });

    globalEvents.on('soundpanel', (msg) => {
      console.log('SOUND : ' + msg);
      const msgParts = msg.split('|');
      if (msgParts.length > 1) {
        console.log('LED ACTION ' + msgParts[0]);
        if (panel['button' + msgParts[0]]) {
          panel['button' + msgParts[0]].led[msgParts[1]]();
        } else {
          console.warn(msgParts[0] + ' doesnt exist on soundpanel');
        }
      } else if (msg == 'allOn') {
        panel.allLedOn();
      } else if (msg == 'allOff') {
        panel.allLedOff();
      }
    });

    this.allLedOff();

    const panel = this;

    this.board.on('exit', function () {
      console.log('Exiting Board');
      panel.button1.stop();
      panel.button2.stop();
      panel.button3.stop();
      panel.button4.stop();
      panel.button5.stop();
      panel.button6.stop();
      panel.button7.stop();
      panel.button8.stop();
      panel.button9.stop();
    });
  }
  allLedOn() {
    console.log('SoundsPanel : All On');
    this.button1.led.on();
    this.button2.led.on();
    this.button3.led.on();
    this.button4.led.on();
    this.button5.led.on();
    this.button6.led.on();
    this.button7.led.on();
    this.button8.led.on();
    this.button9.led.on();
  }
  allLedOff() {
    console.log('SoundsPanel : All Off');
    this.button1.led.off();
    this.button2.led.off();
    this.button3.led.off();
    this.button4.led.off();
    this.button5.led.off();
    this.button6.led.off();
    this.button7.led.off();
    this.button8.led.off();
    this.button9.led.off();
  }
}

module.exports = SoundPanel;
