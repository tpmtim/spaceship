'use strict';
const Mfrc522 = require('mfrc522-rpi');
const SoftSPI = require('rpi-softspi');

const { globalEvents } = require('../utils/global_events.js');

//# This loop keeps checking for chips. If one is near it will get the UID and authenticate
console.log('Keycard Scanner Initialising');

const softSPI = new SoftSPI({
  clock: 23, // pin number of SCLK
  mosi: 19, // pin number of MOSI
  miso: 21, // pin number of MISO
  client: 24, // pin number of CS
});

// GPIO 24 can be used for buzzer bin (PIN 18), Reset pin is (PIN 22).
// I believe that channing pattern is better for configuring pins which are optional methods to use.
const mfrc522 = new Mfrc522(softSPI).setResetPin(22).setBuzzerPin(18);

setInterval(() => {
  //# reset card
  mfrc522.reset();

  //# Scan for cards
  let response = mfrc522.findCard();
  if (!response.status) {
    return;
  }

  //# Get the UID of the card
  response = mfrc522.getUid();
  if (!response.status) {
    console.log('UID Scan Error');
    return;
  }
  //# If we have the UID, continue
  const uid = response.data;
  console.log(
    'Card read UID: %s %s %s %s',
    uid[0].toString(16),
    uid[1].toString(16),
    uid[2].toString(16),
    uid[3].toString(16)
  );

  globalEvents.emit('sendws', 'keycard|' + (uid[0].toString(16) + uid[1].toString(16) + uid[2].toString(16) + uid[3].toString(16)));
}, 500);
