const { globalEvents } = require('../utils/global_events.js');

const shipState = {
  OFF: 'off',
  ON: 'on',
  COUNTDOWN: 'countdown'
};

class Spaceship {
  constructor() {
    this.state = shipState.OFF;
    this.currentVideo = 'waiting';
  }

  tryKeyActivation(keycode) {
    if (keycode === 'aa79') {
      this.turnOn();
    }
  }

  turnOn() {
    this.state = shipState.ON;
    this.currentVideo = 'launch';
    globalEvents.emit('spaceship_update');
  }
}

exports.spaceship = new Spaceship();
