const EventEmitter = require('events').EventEmitter,
  globalEvents = new EventEmitter();

exports.globalEvents = globalEvents;
