const textToSpeech = require('@google-cloud/text-to-speech');
const fs = require('fs');
const util = require('util');

const basePath = '/mnt/c/Dev/spaceship-output/';
const baseConfig = require(basePath + 'baseconfig.json')

const client = new textToSpeech.TextToSpeechClient();

const textConfig = require(basePath + 'texts_' + baseConfig.language);
const namesDirectory = basePath + baseConfig.names.join('_').toLowerCase() + '/';

console.log('Language : ' + baseConfig.language);
console.log('Names : ' + baseConfig.names);
console.log('NamesDirectory : ' + namesDirectory);

const namesString = baseConfig.names.join(' and ');

const checkNamesDirectory = function () {
  if (!fs.existsSync(basePath + baseConfig.names.join('_').toLowerCase())) {
    fs.mkdirSync(namesDirectory);
    return true;
  }
  return false;
}

const saveTextToSpeech = async function (text, outputFile) {

  text = text.replace('{{names}}', namesString);
  text = text.replace('{{randomName}}', baseConfig.names[Math.floor(Math.random() * baseConfig.names.length)])

  const request = {
    input: { text: text },
    voice: { languageCode: 'en-US', name: "en-US-Wavenet-D" },
    audioConfig: {
      audioEncoding: 'MP3',
      pitch: -3.8,
      speakingRate: 1
    },
  };
  const [response] = await client.synthesizeSpeech(request);
  const writeFile = util.promisify(fs.writeFile);
  await writeFile(outputFile, response.audioContent, 'binary');
  console.log(`Audio content written to file: ${outputFile}`);
}

const generateAllTextFiles = async function () {
  for (textKey in textConfig.strings) {
    console.log(textKey);
    await saveTextToSpeech(textConfig.strings[textKey], namesDirectory + textKey + '.mp3')
  }
}

//if (checkNamesDirectory()) {
generateAllTextFiles();
//} else {
//  console.log('Folder already exists')
//}



